#ifndef COMPORT_H
#define COMPORT_H
#include <QCoreApplication>
#include <QSerialPort>
#include <QTextStream>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QVector>
#include <QFile>

class ComPort : public QCoreApplication
{
  Q_OBJECT
private:
  QJsonObject _json;
  QSerialPort* _inst = NULL;
  QString _name;
  QString makeSpacedByteString(const QByteArray &input);
  QVector<QByteArray> vocKey;
  QVector<QByteArray> vocVal;
public:
  ComPort(int argc, char *argv[]);
  ComPort(QString& PortName,int argc, char *argv[]);
  ~ComPort();
  bool isOpen();
  bool Close();
  void Open();
  void setName(QString& PortName);
  void setBaud(QSerialPort::BaudRate baud);
  void setStopBits(QSerialPort::StopBits stopBits);
  void setParity(QSerialPort::Parity parity);
  void setFlowControl(QSerialPort::FlowControl flControl);
  void setDataBits(QSerialPort::DataBits dtBits);
  void makeVocabulary(const QString& JsonPath = "vocabulary.json");
signals:
  void readed(QByteArray&);
public slots:
  void throwAnswer(QByteArray&);
  void recieved();
  void putIntoConsole(QByteArray&);
};

#endif // COMPORT_H
