#include "comport.h"

ComPort::ComPort(int argc, char *argv[]) : QCoreApplication(argc,argv)
{
  _inst = new QSerialPort();

  this->connect(this,SIGNAL(readed(QByteArray&)),this,SLOT(putIntoConsole(QByteArray&)));
  this->connect(this,SIGNAL(readed(QByteArray&)),this,SLOT(throwAnswer(QByteArray&)));
  this->connect(_inst,SIGNAL(readyRead()),this,SLOT(recieved()));
}

ComPort::~ComPort()
{
  Close();
  delete _inst;
  _json.~QJsonObject();
}

void ComPort::setName(QString &PortName)
{
  if(_inst)
  {
    _inst->setPortName(PortName);
    _name = PortName;
  }
}

void ComPort::setBaud(QSerialPort::BaudRate baud)
{
  if(_inst)
  {
    _inst->setBaudRate(baud);
  }
}

void ComPort::setParity(QSerialPort::Parity parity)
{
  if(_inst)
  {
    _inst->setParity(parity);
  }
}

void ComPort::setStopBits(QSerialPort::StopBits StopBit)
{
  if (_inst)
  {
    _inst->setStopBits(StopBit);
  }
}

void ComPort::setFlowControl(QSerialPort::FlowControl flControl)
{
  if(_inst)
  {
    _inst->setFlowControl(flControl);
  }
}

void ComPort::setDataBits(QSerialPort::DataBits dtBits)
{
  if(_inst)
  {
    _inst->setDataBits(dtBits);
  }
}

bool ComPort::isOpen()
{
  if(_inst)
    return _inst->isOpen();
  else
    return false;
}

void ComPort::Open()
{
  try
  {
    QTextStream cout(stdout);
    cout<<":::::::::Trying to connect!:::::::::::"<<endl;
    cout<<"::::::::::::::::::::::::::::::::::::::"<<endl;
    while(!_inst->open(QIODevice::ReadWrite)){};
    cout<<":::::::::::::CONNECTED::::::::::::::::"<<endl;
    cout<<"::::::::::::::::::::::::::::::::::::::"<<endl;
    cout<<":::::::Ready for data recieving:::::::"<<endl;
  }
  catch(...)
  {
    throw QString("There's an error with openning SerialPort!");
  }
}

bool ComPort::Close()
{
  try
  {
    _inst->close();
    return true;
  }
  catch(...)
  {
    throw QString("May be instance of SerialPort doesn't exist");
  }
}

void ComPort::makeVocabulary(const QString& JsonPath)
{
  try
  {
//    QFile file(JsonPath);
//    if(file.open(QIODevice::ReadOnly|QIODevice::Text))
//      _json = QJsonDocument::fromJson(file.readAll()).object();
//    else
//      throw QString("File wasn't opened!");
    QByteArray key1;
    key1.append("\xf0\xf0\x08\x00\xd7\xdf\x07\x01",8);
    QByteArray val1;
    val1.append("\x00\x00\x1A\x00\x00\x00\x04\x02\xFF\x00\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF",26);
    QByteArray key2;
    key2.append("\xf0\xf0\x08\x00\x82\x8c\x04\x01",8);
    QByteArray val2;
    val2.append("\x00\x00\x1A\x00\x00\x00\x04\x02\xFF\x00\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xAD\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF",26);
    vocKey.push_back(key1);
    vocVal.push_back(val1);
    vocKey.push_back(key2);
    vocVal.push_back(val2);
  }
  catch(...)
  {
    throw QString("There's trouble with file");
  }
}

void ComPort::recieved()
{
  try
  {
    QByteArray buff = _inst->readAll();
    emit readed(buff);
  }
  catch(...)
  {
    throw QString("There's an error with transmitting!");
  }
}

void ComPort::putIntoConsole(QByteArray& buff)
{
    QTextStream cout(stdout);
    cout<<endl<<"Package has been recieved -> "<<makeSpacedByteString(buff)<<endl<<"size = "<<buff.length()<<endl;
}

void ComPort::throwAnswer(QByteArray& buff)
{
  try
  {
    QTextStream cout(stdout);
//    QByteArray ans(_json.value("x"+makeSpacedByteString(buff)).toVariant().toByteArray());
//    char* charAns = ans.data();
//    int sizechart = ans.length();
    QByteArray ans;

    for(int i=0;i<vocKey.length();i++)
    {
      if(vocKey[i] ==buff)
      {
        if(vocVal[i].length()==26)
        {
          vocVal[i][10] = abs(qrand()%100);
          vocVal[i][11] = abs(qrand()%100);
          vocVal[i][12] = abs(qrand()%100);
          vocVal[i][13] = abs(qrand()%100);
          vocVal[i][14] = abs(qrand()%100);
          vocVal[i][15] = abs(qrand()%100);
          vocVal[i][16] = abs(qrand()%100);
          vocVal[i][17] = abs(qrand()%100);
          vocVal[i][18] = abs(qrand()%100);
          vocVal[i][19] = abs(qrand()%100);
          vocVal[i][20] = abs(qrand()%100);
          vocVal[i][21] = abs(qrand()%100);
          vocVal[i][22] = abs(qrand()%100);
          vocVal[i][23] = abs(qrand()%100);
          vocVal[i][24] = abs(qrand()%100);
          vocVal[i][25] = abs(qrand()%100);
        }
        ans = vocVal[i];
      }
    }
    if(!_inst->write(ans))
      cout<<"Warning::::::Answer wasn't found::::::"<<endl;
    else
      cout<<"Such answer was threw -------> "<<makeSpacedByteString(ans)<<endl;
  }
  catch(...)
  {
    throw QString("There's trouble with vocabulary");
  }
}

QString ComPort::makeSpacedByteString(const QByteArray &input)
{
  QString result(input.toHex());
  int shift = 0;

  for (int i = 2; i < result.size() - shift; i += 2, ++shift){
    result.insert(i + shift, 'x');
  }

  return result;
}

