
#include <comport.h>
QTextStream cout(stdout);
QTextStream cin(stdin);



int main(int argc, char *argv[])
{
  ComPort a(argc,argv);
  QString name;
  cout<<"Enter comPort name ---> "<<endl;
  cin>>name;
  QString jSonName("C:/Users/BloodyReaper/Documents/QSerialPortTester/document.json");
  try
  {
    a.makeVocabulary(jSonName);
    a.setName(name);
    a.setBaud(QSerialPort::BaudRate::Baud115200);
    a.setParity(QSerialPort::NoParity);
    a.setStopBits(QSerialPort::OneStop);
    a.setDataBits(QSerialPort::Data8);
    a.Open();
    a.exec();
  }
  catch(QString message)
  {
    cout<<message<<endl;
    system("pause");
    return 0;
  }
}
